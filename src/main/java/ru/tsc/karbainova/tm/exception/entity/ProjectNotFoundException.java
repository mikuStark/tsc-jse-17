package ru.tsc.karbainova.tm.exception.entity;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {
    public ProjectNotFoundException() {
        super("Error Project not found.");
    }
}
