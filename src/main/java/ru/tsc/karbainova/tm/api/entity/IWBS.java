package ru.tsc.karbainova.tm.api.entity;

public interface IWBS extends IHasDateCreated, IHasStartDate, IHasStatus, IHasName {
}
